<?php

// Simple style
const FromValue = 1;
const ToValue = 50;

function findFiveOrSevenDivision($number)
{
    if (($number % 5) === 0) {
        return ($number % 7) === 0 ? 'данет' : 'да';
    }
    if (($number % 7) === 0) {
        return 'нет';
    }

    return $number;
}

function outputValues()
{
    foreach (range(FromValue, ToValue) as $number) {
        echo findFiveOrSevenDivision($number) . "\n";
    }
}

outputValues();


// Via generator
function outputYesNoValues($start, $limit, $step = 1)
{
    for ($number = $start; $number <= $limit; $number += $step) {
        yield findFiveOrSevenDivision($number);
    }
}

foreach (outputYesNoValues(FromValue, ToValue) as $phrase) {
    echo "$phrase ";
}

